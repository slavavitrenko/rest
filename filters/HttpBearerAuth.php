<?php

namespace slavavitrenko\rest\filters;

use Yii;
use yii\web\UnauthorizedHttpException;


class HttpBearerAuth extends \yii\filters\auth\HttpBearerAuth
{

    public $roles = [];

    public $except;


    public function authenticate($user, $request, $response)
    {
        $authHeader = $request->getHeaders()->get('Authorization');


        if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {

            $identity = $user->loginByAccessToken($matches[1], get_class($this));

            if (($identity === null) or (Yii::$app->user->identity->token_expire < time())) {
                $this->handleFailure($response);
            }
            else{
                if((!in_array('@', $this->roles)) && !in_array(Yii::$app->identity->type, $this->roles)){
                    $this->handleFailure($response);
                }

                $identity->updateAttributes([
                   'token_expire' =>  time() + 7200,
                ]);
            }
            return $identity;
        }

        return null;
    }

    public function handleFailure($response)
    {
        throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
    }

}