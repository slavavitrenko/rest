<?php

namespace slavavitrenko\rest;

use Yii;
use yii\data\ActiveDataProvider;


class IndexAction extends Action
{

    public $prepareDataProvider;

    public $searchModelClass = null;

    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        return $this->prepareDataProvider();
    }

    protected function prepareDataProvider()
    {
        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }

        if($this->searchModelClass){
            $searchModelClass = $this->searchModelClass;
            $searchModel = new $searchModelClass;

            $data = Yii::$app->request->get();
            if(empty($data)){
                $data = Yii::$app->request->post();
            }

            $dataProvider = $searchModel->search($data, '');
            return [
                'errors' => $searchModel->errors,
                'data' => $dataProvider->getModels(),
                'per-page' => $dataProvider->pagination->pageSize,
                'totalCount' => $dataProvider->totalCount,
                'page-count' => round($dataProvider->totalCount / $dataProvider->pagination->pageSize, 0),
            ];
        }

        $modelClass = $this->modelClass;

        return new ActiveDataProvider([
            'query' => $modelClass::find(),
        ]);
    }


}
