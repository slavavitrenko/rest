<?php

namespace slavavitrenko\rest;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\ForbiddenHttpException;
use slavavitrenko\rest\filters\HttpBearerAuth;


class ActiveController extends Controller
{

    public $roles;

    public $except = [];

    public $searchModelClass = null;

    public $modelClass;

    public $updateScenario = Model::SCENARIO_DEFAULT;

    public $createScenario = Model::SCENARIO_DEFAULT;

    public $methods = [
        'GET',
        'HEAD',
        'OPTIONS',
        'PUT',
        'PATCH',
        'POST',
        'DELETE',
    ];

    public function init(){
        parent::init();

        if ($this->modelClass === null) {
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }
        if ($this->roles === null) {
            throw new InvalidConfigException('The "roles" property must be set.');
        }
        if(Yii::$app->request->method === 'OPTIONS'){
            $this->options();
            Yii::$app->end();
        }
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        if(in_array('?', $this->roles)){
            return $behaviors;
        }

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'roles' => $this->roles,
            'except' => $this->except,
        ];
        return $behaviors;
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'slavavitrenko\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'searchModelClass' => $this->searchModelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'slavavitrenko\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'slavavitrenko\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'slavavitrenko\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => 'slavavitrenko\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'slavavitrenko\rest\OptionsAction',
            ],
        ];
    }

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        
    }

    private function options(){
        $options = $this->methods;
        Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $options));
    }

}
