<?php

namespace slavavitrenko\rest;

use Yii;

class OptionsAction extends \yii\base\Action
{

    public $collectionOptions = ['GET', 'POST', 'HEAD', 'OPTIONS'];

    public $resourceOptions = ['GET', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'];



    public function run($id = null)
    {
        if (Yii::$app->getRequest()->getMethod() !== 'OPTIONS') {
            Yii::$app->getResponse()->setStatusCode(405);
        }
        $options = $id === null ? $this->collectionOptions : $this->resourceOptions;
        Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $options));
    }
    
}
