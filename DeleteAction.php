<?php


namespace slavavitrenko\rest;

use Yii;
use yii\web\ServerErrorHttpException;


class DeleteAction extends Action
{

    public function run($id)
    {
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        if (($count = $model->delete()) === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
        else{
            Yii::$app->getResponse()->setStatusCode(204);
            return[
                'count' => $count,
                'errors' => $model->errors,
            ];
        }

    }

}